# Exercício 11

# Altere o programa anterior, intercalando 3 vetores de 10 elementos cada.

vetor1 = []
vetor2 = []
vetor3 = []
vetorMisto = []

for i in range(3):
    
    num1 = int(input(f'Digite o {i+1}º número do primeiro vetor: '))
    vetor1.append(num1)
    vetorMisto.append(num1)

    num2 = int(input(f'Digite o {i+1}º número do segundo vetor: '))
    vetor2.append(num2)
    vetorMisto.append(num2)

    num3 = int(input(f'Digite o {i+1}º número do terceiro vetor: '))
    vetor3.append(num3)
    vetorMisto.append(num3)

print(
      f'Vetor intercalado: {vetorMisto}\n'
      f'Vetor 1: {vetor1}\n'
      f'Vetor 2: {vetor2}\n'
      f'Vetor 3: {vetor3}\n'
     )