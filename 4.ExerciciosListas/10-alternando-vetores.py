# Exercício 10

# Faça um Programa que leia dois vetores com 10 elementos cada. 
# Gere um terceiro vetor de 20 elementos, cujos valores deverão ser 
# compostos pelos elementos intercalados dos dois outros vetores.


vetor1 = []
vetor2 = []
vetorMisto = []

for i in range(10):
    num1 = int(input(f'Digite o {i+1}º número do primeiro vetor: '))
    vetor1.append(num1)
    vetorMisto.append(num1)

    num2 = int(input(f'Digite o {i+1}º número do segundo vetor: '))
    vetor2.append(num2)
    vetorMisto.append(num2)

print(
      f'Vetor intercalado: {vetorMisto}\n'
      f'Vetor 1: {vetor1}\n'
      f'Vetor 2: {vetor2}\n'
     )