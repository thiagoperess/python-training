# Exercício 12

# Foram anotadas as idades e alturas de 30 alunos. 
# Faça um Programa que determine quantos alunos com mais de 13 anos 
# possuem altura inferior à média de altura desses alunos.

alturas = []
alunos = 0

for i in range(30):
    idade = int(input(f'Idade: '))
    altura = float(input(f'Altura: '))
    alturas.append(altura)

    mediaAltura = sum(alturas) / len(alturas)

    if idade > 13 and altura > mediaAltura:
        alunos += 1


print(
      f'As alturas são {alturas}\n'
      f'A média das alturas é: {mediaAltura:.2f}\n'
      f'Alunos com mais de 13 anos com altura acima da média: {alunos}'
      )
